//Funcion para genra color hex random
function hexColorGenerator() {
    var generatedColor = "";

    for ( i = 0; i < 3; i++) {
        var randomNumber = Math.floor(Math.random()*255);
        if (randomNumber < 16) {
            generatedColor = generatedColor + "0" + randomNumber.toString(16);
        } else {
            generatedColor = generatedColor + randomNumber.toString(16)
        }
    }

    return ('#' + generatedColor);
}

// let hexColorGenerator() = '#' + Math.random().toString(16).slice(2,8)
// let incorrectRandomHex = '#' + Math.random().toString(16).slice(2,8)
// Genera un numero para el color correcto
let correctColor = Math.floor(Math.random() * 4)

console.log(correctColor)

document.querySelector('#hexCode').innerHTML = hexColorGenerator()

if(correctColor === 1){
    //Mostrar color
    document.querySelector('#firstOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#secondOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#thirdOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#fourthOption').style.backgroundColor = hexColorGenerator()
    //Alertas
    document.querySelector('#firstOption').addEventListener('click',function(){
        Notiflix.Report.Success(
            '¡Felicidades! Es el color correcto',
            '¡Vamos por otro color!',
            'Click',
            function(){ //Reincia la app si el color es correcto
                location.reload()
            }
        )
    })
    document.querySelector('#secondOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
    document.querySelector('#thirdOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
    document.querySelector('#fourthOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
} else if(correctColor === 2) {
    //Mostrar color
    document.querySelector('#firstOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#secondOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#thirdOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#fourthOption').style.backgroundColor = hexColorGenerator()
    //Alertas
    document.querySelector('#secondOption').addEventListener('click',function(){
        Notiflix.Report.Success(
            '¡Felicidades! Es el color correcto',
            '¡Vamos por otro color!',
            'Click',
            function(){ //Reincia la app si el color es correcto
                location.reload()
            }
        )
    })
    document.querySelector('#firstOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
    document.querySelector('#thirdOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
    document.querySelector('#fourthOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
} else if(correctColor === 3) {
    //Mostrar color
    document.querySelector('#firstOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#secondOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#thirdOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#fourthOption').style.backgroundColor = hexColorGenerator()
    //Alertas
    document.querySelector('#thirdOption').addEventListener('click',function(){
        Notiflix.Report.Success(
            '¡Felicidades! Es el color correcto',
            '¡Vamos por otro color!',
            'Click',
            function(){ //Reincia la app si el color es correcto
                location.reload()
            }
        )
    })
    document.querySelector('#firstOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
    document.querySelector('#secondOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
    document.querySelector('#fourthOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
} else {
    //Mostrar color
    document.querySelector('#firstOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#secondOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#thirdOption').style.backgroundColor = hexColorGenerator()
    document.querySelector('#fourthOption').style.backgroundColor = hexColorGenerator()
    //Alertas
    document.querySelector('#fourthOption').addEventListener('click',function(){
        Notiflix.Report.Success(
            '¡Felicidades! Es el color correcto',
            '¡Vamos por otro color!',
            'Click',
            function(){ //Reincia la app si el color es correcto
                location.reload()
            }
        )
    })
    document.querySelector('#firstOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
    document.querySelector('#secondOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
    document.querySelector('#thirdOption').addEventListener('click', function(){
        Notiflix.Notify.Failure('No es el color correcto')
    })
}
